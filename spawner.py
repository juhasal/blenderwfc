import bpy
import random
import numpy

## Load objects

C = bpy.context
base = bpy.data.objects["base"]
baseVertical = bpy.data.objects["baseVertical"]
baseVerticalTop = bpy.data.objects["baseVertical2"]
east = bpy.data.objects["east"]
north = bpy.data.objects["north"]
west = bpy.data.objects["west"]
south = bpy.data.objects["south"]
empty = bpy.data.objects["empty"]
emptyTerminator = bpy.data.objects["emptyTerminator"]
curveNE = bpy.data.objects["curveNE"]
curveNW = bpy.data.objects["curveNW"]
curveSE = bpy.data.objects["curveSE"]
curveSW = bpy.data.objects["curveSW"]



## Set tile connections:
## 1: full
## 2: empty
## 3: half side
## 4: vertical connector?
## -1: no connection for anyone
## directions: north, west, south, east, up, down

connections = {
    base: {
        "north":1,
        "west":1,
        "south":1,
        "east":1,
        "up":1,
        "down":1
    },
    baseVertical: {
        "north":2,
        "west":2,
        "south":2,
        "east":2,
        "up":1,
        "down":1
    },
    baseVerticalTop: {
        "north":2,
        "west":2,
        "south":2,
        "east":2,
        "up":2,
        "down":1
    },
    east: {
        "north":3,
        "west":1,
        "south":3,
        "east":2,
        "up":-1,
        "down":-1
    },
    north: {
        "north":2,
        "west":3,
        "south":1,
        "east":3,
        "up":-1,
        "down":-1
    },
    west: {
        "north":3,
        "west":2,
        "south":3,
        "east":1,
        "up":-1,
        "down":-1
    },
    south: {
        "north":1,
        "west":3,
        "south":2,
        "east":3,
        "up":-1,
        "down":-1
    },
    empty: {   
        "north":2,
        "west":2,
        "south":2,
        "east":2,
        "up":2,
        "down":2
    },
    emptyTerminator: {
        "north":1,
        "west":1,
        "south":1,
        "east":1,
        "up":2,
        "down":2
    },
    curveNE: {
        "north":1,
        "west":2,
        "south":2,
        "east":1,
        "up":-1,
        "down":-1
    },
    curveSW: {
        "north":2,
        "west":1,
        "south":1,
        "east":2,
        "up":-1,
        "down":-1
    },
     curveSE: {
        "north":2,
        "west":2,
        "south":1,
        "east":1,
        "up":-1,
        "down":-1
    },
    curveNW: {
        "north":1,
        "west":1,
        "south":2,
        "east":2,
        "up":-1,
        "down":-1
    },
}


##
## GENERATE RULES FROM CONNECTIONS
##
## Check all tile connections for all directions for all tiles
## ex. If a tile "down" connection is the same as tile "up" connection, append tile to the rules "down"-array 
##
## Notes: Multiple possible connections? any(i in a for i in b)
##

## Note: emptyTerminator disabled
allParts = [base, east, north, west, south, empty, curveNE, curveSE, curveNW, curveSW, baseVertical, baseVerticalTop]
directions = ["north", "west", "south", "east", "up", "down"]
newRules = {}

for currentPart in allParts:
    newRules[currentPart] = {"north":[], "west":[], "south":[], "east":[], "up":[], "down":[]}
    for comparedPart in allParts:
        if(connections[currentPart]["north"] == connections[comparedPart]["south"] and connections[currentPart]["north"] > 0 ):
            newRules[currentPart]["north"].append(comparedPart)
        if(connections[currentPart]["south"] == connections[comparedPart]["north"] and connections[currentPart]["south"] > 0):
            newRules[currentPart]["south"].append(comparedPart)
        if(connections[currentPart]["east"] == connections[comparedPart]["west"] and connections[currentPart]["east"] > 0):
            newRules[currentPart]["east"].append(comparedPart)
        if(connections[currentPart]["west"] == connections[comparedPart]["east"] and connections[currentPart]["west"] > 0):
            newRules[currentPart]["west"].append(comparedPart)
        if(connections[currentPart]["up"] == connections[comparedPart]["down"] and connections[currentPart]["up"] > 0):
            newRules[currentPart]["up"].append(comparedPart)
        if(connections[currentPart]["down"] == connections[comparedPart]["up"] and connections[currentPart]["down"] > 0):
            newRules[currentPart]["down"].append(comparedPart)


#print(newRules)
rules = newRules

##
## Grid dimensions
##

DIM = 6
HEIGHT = 4
TotalLength = DIM*DIM*HEIGHT

##
## Initialize grid 
##
class Part:
    def __init__(self, collapsed, options, mesh):
        self.collapsed = collapsed
        self.options = options
        self.mesh = mesh

part1 = Part(False, allParts, base)
        
grid = []
for k in range(HEIGHT):
    for i in range(DIM):
        for j in range(DIM):
            grid.append(Part(False, allParts, None))
        
def optionsLength(e):
    return len(e.options)

##
## Check rules and update options according to collapsed cells
##
def checkRules(grid):
    for k in range(HEIGHT):
        for i in range(DIM):
            for j in range(DIM):
                #Neighbour cells, check edge cases:
                
                gridCopy = grid.copy()
                currentCell = grid[j+i*DIM+k*DIM*DIM]
                southCell = None
                northCell = None
                westCell = None
                eastCell = None
                upCell = None
                downCell = None

                if(i > 0): westCell = gridCopy[j+(i-1)*DIM+k*DIM*DIM]
                if(i < DIM-1): eastCell = gridCopy[j+(i+1)*DIM+k*DIM*DIM]
                if(j > 0): southCell = gridCopy[j-1+i*DIM+k*DIM*DIM]
                if(j < DIM-1): northCell = gridCopy[j+1+i*DIM+k*DIM*DIM]
                if(k < HEIGHT-1): upCell = gridCopy[j+i*DIM+(k+1)*DIM*DIM]
                if(k > 0): downCell = gridCopy[j+i*DIM+(k-1)*DIM*DIM]

                #Check common cells in arrays with set intersection
                if(southCell != None and southCell.mesh != None): currentCell.options = list(set(currentCell.options).intersection(set(rules[southCell.mesh]["north"])))
                if(eastCell != None and eastCell.mesh != None): currentCell.options = list(set(currentCell.options).intersection(set(rules[eastCell.mesh]["west"])))
                if(northCell != None and northCell.mesh != None): currentCell.options = list(set(currentCell.options).intersection(set(rules[northCell.mesh]["south"])))
                if(westCell != None and westCell.mesh != None): currentCell.options = list(set(currentCell.options).intersection(set(rules[westCell.mesh]["east"])))
                if(downCell != None and downCell.mesh != None): currentCell.options = list(set(currentCell.options).intersection(set(rules[downCell.mesh]["up"])))
                if(upCell != None and upCell.mesh != None): currentCell.options = list(set(currentCell.options).intersection(set(rules[upCell.mesh]["down"])))
            
##
# Select a cell with the lowest entropy, select a part at random from available options, and collapse it.
# Entropy is the number of possible options, collapsing the cell means selecting the mesh and setting the options to the selected mesh
##
def evaluate(grid):
    gridCopy = []
    altGrid = grid.copy()
    
    for i in range(len(altGrid)):
        if(altGrid[i].collapsed == False):
            gridCopy.append(altGrid[i])
            
    
    gridCopy.sort(key=optionsLength)
    minimumLength = len(gridCopy[0].options)
    
    for k in range(HEIGHT):
        for i in range(DIM):
            for j in range(DIM):
                if(len(grid[j+i*DIM+k*DIM*DIM].options)==minimumLength and grid[j+i*DIM+k*DIM*DIM].collapsed == False):
                    if(len(grid[j+i*DIM+k*DIM*DIM].options) > 0): grid[j+i*DIM+k*DIM*DIM].mesh = random.choice(grid[j+i*DIM+k*DIM*DIM].options)
                    else: grid[j+i*DIM+k*DIM*DIM].mesh = empty
                    grid[j+i*DIM+k*DIM*DIM].options = [grid[j+i*DIM+k*DIM*DIM].mesh]
                    grid[j+i*DIM+k*DIM*DIM].collapsed = True
                    break
            else:  # only execute when there's no break in the inner loop
                continue
            break
        else:
            continue
        break
    
# Build function:
#  -If a cell is collapsed, copy the selected mesh and place it.
   
def build(grid):
    for k in range(HEIGHT):
        for i in range(DIM):
            for j in range(DIM):
                if(grid[j+i*DIM+k*DIM*DIM].collapsed):
                    mesh = grid[j+i*DIM+k*DIM*DIM].mesh
                    obj = mesh.copy()
                    obj.data = mesh.data.copy()
                    C.collection.objects.link(obj)
                    obj.location[0] = i
                    obj.location[1] = j
                    obj.location[2] = k

# Run program

for x in range(TotalLength-1):
    checkRules(grid)
    evaluate(grid)

build(grid)

print("***END***")
